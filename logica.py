
# Conexiones 

from cnx_sqlserver import conexion
from cnx_mysql import my_conexion

print('---------------------')
print('::: YAK INDUSTRYS :::')
print('---------------------')
print('...Conexion Doble Enlazada.')

print()
print('Consulta SQL Server') 
print()

try:
    with conexion.cursor() as cursor:
        
        cursor.execute('SELECT id,nomCurso FROM tb_curso;')
        
        datos = cursor.fetchall()
        
        for dato in datos:
            print(dato)
    
except Exception as e:
    print('Error en la Consulta :',e)
finally:
    conexion.close()
    
print()
print('Consulta MySQL') 
print()

try:
    with my_conexion.cursor() as mycursor:
    
        mycursor.execute('SELECT id,nombre FROM peliculas;')
        
        mydatos = mycursor.fetchall()
        
        for mydato in mydatos:
            print(mydato)
            
except Exception as e:
    print('Error de Consulta :',e)
finally:
    my_conexion.close()
    
print()
print('...terminar procesos')
print()